angular.module('starter.controllers', [])

.controller('HomeCtrl', function($scope, $rootScope, $http, $location) {
  console.log('home');

  $scope.getData = function() {
    $http.post('https://29d9t2e36f.execute-api.us-west-2.amazonaws.com/test/client', JSON.stringify({location: 'AEM Station'}))
      .then(function(data) {
        $rootScope.data = data.data;
      }, function(err) {
        console.log(err);
      });
  };

  $scope.viewLocation = function() {
    $location.path('/location');
  }

  $scope.getData();
})

.controller('LocationCtrl', function($scope, $rootScope, $stateParams, $location) {
  console.log('Location');
  console.log($rootScope.data);

  $scope.viewDevice = function(device) {
    $rootScope.device = device;
    $location.path('/device');
  }
})

.controller('DeviceCtrl', function($scope, $rootScope, $location) {

  console.log('Selected device -->');
  console.log($rootScope.device);

  $scope.hourSegments = [
    {label : '00:00 - 01:00', show : false},
    {label : '01:00 - 02:00', show : false},
    {label : '02:00 - 03:00', show : false},
    {label : '03:00 - 04:00', show : false},
    {label : '04:00 - 05:00', show : false},
    {label : '05:00 - 06:00', show : false},
    {label : '06:00 - 07:00', show : false},
    {label : '07:00 - 08:00', show : false},
    {label : '08:00 - 09:00', show : false},
    {label : '09:00 - 10:00', show : false},
    {label : '10:00 - 11:00', show : false},
    {label : '11:00 - 12:00', show : false},
    {label : '12:00 - 13:00', show : false},
    {label : '13:00 - 14:00', show : false},
    {label : '14:00 - 15:00', show : false},
    {label : '15:00 - 16:00', show : false},
    {label : '16:00 - 17:00', show : false},
    {label : '17:00 - 18:00', show : false},
    {label : '18:00 - 19:00', show : false},
    {label : '19:00 - 20:00', show : false},
    {label : '20:00 - 21:00', show : false},
    {label : '21:00 - 22:00', show : false},
    {label : '22:00 - 23:00', show : false},
    {label : '23:00 - 00:00', show : false}
  ];
  $scope.minuteSegments = [
    {label : ':00 - :14', show : false},
    {label : ':15 - :29', show : false},
    {label : ':30 - :44', show : false},
    {label : ':45 - :59', show : false}
  ];

  var datePickerCallback = function (val) {
    if (typeof(val) === 'undefined') {
      console.log('No date selected');
    } else {
      $scope.datepickerObject.inputDate = val;
      $scope.selectedDate = val;

      $scope.loadDayData();
    }
  };

  $scope.datepickerObject = {
    //titleLabel: 'Title',  //Optional
    todayLabel: 'Today',  //Optional
    closeLabel: 'Close',  //Optional
    setLabel: 'Set',  //Optional
    setButtonType : 'button-assertive',  //Optional
    todayButtonType : 'button-assertive',  //Optional
    closeButtonType : 'button-assertive',  //Optional
    inputDate: new Date(),  //Optional
    mondayFirst: false,  //Optional
    //disabledDates: disabledDates, //Optional
    //weekDaysList: weekDaysList, //Optional
    //monthList: monthList, //Optional
    templateType: 'popup', //Optional
    showTodayButton: 'true', //Optional
    modalHeaderColor: 'bar-positive', //Optional
    modalFooterColor: 'bar-positive', //Optional
    from: new Date(2012, 8, 2), //Optional
    to: new Date(),  //Optional
    callback: function (val) {  //Mandatory
      datePickerCallback(val);
    },
    dateFormat: 'MM-dd-yyyy', //Optional
    closeOnSelect: false, //Optional
  };

  $scope.dailyAvg = 0;
  $scope.selectedDate = new Date(); // default to current day
  $scope.dayData;

  $scope.loadDayData = function() {
    var calDate = $scope.selectedDate.getFullYear() + '-' + ("0" + ($scope.selectedDate.getMonth() + 1)).slice(-2) + '-' + $scope.selectedDate.getDate();

    var dayAvg = 0;

    var dt;
    for (date in $rootScope.device.dates) {
      if ($rootScope.device.dates[date].date == calDate) { // search for the selected date in the data
        $scope.dayData = JSON.parse(JSON.stringify($rootScope.device.dates[date]));
        var hourCount = 0;

        if ($rootScope.device.dataType == 'number') { // if metric data, then calculate metrics
          for (hour in $scope.dayData.hours) { // go through hours to calculate averages
            var minCount = 0;
            var total = 0;
            var quarterAverage = 0;
            var averages = []; // for 15 minute averages
            for (minute in $scope.dayData.hours[hour]) { // add up each minute's data
              if ($scope.dayData.hours[hour][minute]) {

                total += +$scope.dayData.hours[hour][minute].split(' ')[0];
                quarterAverage += +$scope.dayData.hours[hour][minute].split(' ')[0];
      
                if (++minCount % 15 == 0) {
                  averages.push((quarterAverage / 15).toFixed(2));
                  quarterAverage = 0;
                }
              }
            }
            if ($scope.dayData.hours[hour]) {
              $scope.dayData.hours[hour].avg = (total / minCount).toFixed(2);
              $scope.dayData.hours[hour].averages = averages;
              dayAvg += +$scope.dayData.hours[hour].avg;
              hourCount++;
            }
          }

          $scope.dailyAvg = (dayAvg / hourCount).toFixed(2);
          console.log("day avg: " + $scope.dailyAvg);
        }

        break;
      }
    }
  };

  $scope.toggleGroup = function(group) {
    if (group.show == undefined) {
      group.show = false;
    }
    group.show = !group.show;
  };
  $scope.isGroupShown = function(group) {
    return group.show;
  };
  $scope.range = function(min, max, step) {
    step = step || 1;
    var input = [];
    for (var i = min; i <= max; i += step) {
        input.push(i);
    }
    return input;
  };

  $scope.loadDayData();

});
